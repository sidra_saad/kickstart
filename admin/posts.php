<?php
	require_once('index.php');


class Posts extends Adminpanel{

			public function __construct(){
						parent::__construct();
						if (!empty($_GET['action'])) {
								switch ($_GET['action']) {
									case 'create':
									$this->addPost();
									break;
									case 'edit':
									$this->editPosts();
									break;
									default:
									$this->listPosts();
									break;
									case 'save':
									$this->savePost();
									break;
									case 'delete':
									$this->deletePost();
									break;
														}
						} else {
						$this->listPosts();
						}
			}

			public function listPosts(){

				$posts = $return = array();
				$query = $this->ksdb->db->prepare("SELECT * FROM posts");
				try {
				$query->execute();
				for ($i = 0; $row = $query->fetch(); $i++) {
				$return[$i] = array();
				foreach ($row as $key => $rowitem) {
				$return[$i][$key] = $rowitem;
				}
				}
				} catch (PDOException $e) {
				echo $e->getMessage();
				}
				$posts = $return;
				require_once 'templates/manageposts.php';
			}

			public function editPosts(){

					if(!empty($_GET['id']) && is_numeric($_GET['id'])){
					$post = $this->ksdb->dbselect('posts', array('*'),array('id' => $_GET['id']));
					if(!empty($post) && $post > 0){
						require_once('templates/newpost.php');
					}else{
						header("Location: http://localhost/kickstart/admin/posts.php?status=error");
					}
				}
			}

			public function addPost(){
				require_once 'templates/newpost.php';
			}

			public function savePost(){
				$status='';
				$array = $format = $return = array();

				if (!empty($_POST['post'])) {
					$post = $_POST['post'];
				}

				if (!empty($post['content'])) {
					$array['content'] = $post['content'];
					$format[] 		  = ':content';
				}

			if(!empty($post['title'])){
							$array['title'] = $post['title'];
							$format[] = ':title'; 
						} 


				$cols = $values = '';
				$i = 0;

				foreach ($array as $col => $data) {

					if ($i == 0) {
						$cols 	.= $col;
						$values .= $format[$i];
					} else {
						$cols 	.= ',' . $col;
						$values .= ',' . $format[$i];
					}
					$i++;
				}
				// INSERT INTO posts(content, date_updated) VALUES (:content, :date_updated)
				try {
					$query = $this->ksdb->db->prepare("INSERT INTO posts(".$cols.") VALUES (".$values.")");

					for($c=0;$c<$i;$c++){
						$query->bindParam($format[$c], ${'var'.$c});
					}

					$z=0;

					foreach($array as $col => $data){
						${'var' . $z} = $data;
						$z++;
					}

					$result = $query->execute();
					$add = $query->rowCount();//number of rows affected from last executed query
				} 

				catch (PDOException $e) {
				echo $e->getMessage();
				}

				$query->closeCursor();

				$this->ksdb->db = null;

				if (!empty($add)) {
					$status = array('success' => 'Your post has been saved successfully.');
					
				} else {
					$status = array('error' => 'There has been an error saving your post. Please try again later.');
					
				}
				header("Location: http://localhost/kickstart/admin/posts.php");
			}

			public function deletePost(){	

						if (!empty($_GET['id']) && is_numeric($_GET['id'])) {
						$delete = $this->ksdb->dbdelete('posts', array('id' => $_GET['id']));
						if (!empty($delete) && $delete > 0) {
						header("Location: " . $this->base->url ."/admin/posts.php?delete=success");
						} else {
						header("Location: " . $this->base->url ."/admin/posts.php?delete=error");
												}
						}
			}

}

$admin_posts = new Posts;