	<?php
	require_once('index.php');
class Comments extends Adminpanel{

		public function __construct(){
					parent::__construct();
					if (!empty($_GET['action']) && $_GET['action'] == 'delete') {
								$this->deleteComment();
					} else {
								$this->listComments();
							}
}

public function listComments(){

		$comments = $return = array();
		$query = $this->ksdb->db->prepare("SELECT * FROM comments");
		try {
		$query->execute();
		for ($i = 0; $row = $query->fetch(); $i++) {
		$return[$i] = array();
		foreach ($row as $key => $rowitem) {
		$return[$i][$key] = $rowitem;
		}
		}
		} catch (PDOException $e) {
		echo $e->getMessage();
		}
		$comments = $return;
		require_once 'templates/managecomments.php';
}


public function deleteComment(){
if (!empty($_GET['id']) && is_numeric($_GET['id'])) {
				$delete = $this->ksdb->dbdelete('comments', array('id' => $_GET['id']));
				if(!empty($delete) && $delete > 0){
				header("Location: ".$this->base->url."/admin/comments.php?delete=success");
				}else{
				header("Location: ".$this->base->url."/admincomments.php?delete=error");
				}
				}

}


}

$admin_comments = new Comments;